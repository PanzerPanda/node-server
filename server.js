const express = require('express'),
    cors = require("cors"),
    nodemailer = require('nodemailer'),
    app = express(),
    bodyParser = require("body-parser"),
const port = 3080;


var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'j2acidservices@gmail.com',
        pass: 'super_pass_word1'
    }
});


//configure the Express middleware to accept CORS requests and parse request body into JSON
app.use(cors({ origin: "*" }));
app.use(bodyParser.json());
app.use(express.static(process.cwd()+"/www/"));

app.post('/email/requestquote', (req, res) => {
    var mailOptions = {
        from: `${req.body.email.name} <${req.body.email.email}>`,
        to: 'Jesus.R.Garcia9191@gmail.com',
        subject: 'Quote for ' + req.body.email.name,
        text: req.body.email.body
    };
    console.log(mailOptions);
    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });
    res.json('Email Sent!');
});

app.get('/', (req, res) => {
    res.send('App is working!');
});

app.listen(port, () => {
    console.log(`Server listening on the port ::${port}`);
});